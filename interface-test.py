from random import choice
from tkinter import *
from tkinter import ttk

breakfastmenu = ['五穀飯','ポテトサラダ','お粥','抹茶ケーキ','クリームケーキ','サバ定食','バームクーヘンとコーヒー']
launchmenu = ['ローストチキン','天婦羅','マーポートーフ','カレーライス','炊き込みご飯','丼物','ハンバーガー']
dinnermenu = ['納豆かけご飯','カレーうどん','フライパン','寿司','ラーメン','焼きそば','唐揚げ定食']

testwindow = Tk()
testwindow.title('ネット・キャプター')

frame1 = ttk.Frame(testwindow)
label1 = ttk.Label(frame1, text='どれにしますか？\n朝食(a)・昼食(b)・夕食(c)\n')

t = StringVar()

entry1 = ttk.Entry(frame1, textvariable=t)

def kekkb():
	
	i=input("ほかの料理にしますか？\ny or n\n")
	if i=="y":
		kekka()
	elif i=="n":
		print("ご利用ありがとうございます")

def kekka():

	time=t.get()

	if time=="a":
		print(choice(breakfastmenu),"がお薦めです")
		kekkb()
	elif time=="b":
		print(choice(launchmenu),"がお薦めです")
		kekkb()		
	elif time=="c":
		print(choice(dinnermenu),"がお薦めです")
		kekkb()
	else:
		print("指示通りに入力してくださいね")


	

button1 = ttk.Button(frame1, text='OK', command=kekka)

frame1.grid(row=0,column=0,sticky=(N,E,S,W))
label1.grid(row=1,column=1,sticky=E)
entry1.grid(row=1,column=2,sticky=W)
button1.grid(row=2,column=2,sticky=W)



for child in frame1.winfo_children():
    child.grid_configure(padx=5, pady=5)



testwindow.mainloop()