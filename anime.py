import tkinter 
import tkinter.ttk 
from tkinter.font import Font
from PIL import Image,ImageTk

def create_frame1(): 
	window1_label = tkinter.ttk.Label(first_frame, text='石さんのゲーム') 
	window1_label.grid(column=0, row=0, pady=10, padx=10, sticky=(tkinter.N)) 

	window1_quit_button = tkinter.Button(first_frame, text = "Quit", command = quit_program) 
	window1_quit_button.grid(column=0, row=1, pady=10, sticky=(tkinter.N)) 
	window1_next_button = tkinter.Button(first_frame, text = "Next", command = call_frame2) 
	window1_next_button.grid(column=1, row=1, pady=10, sticky=(tkinter.N)) 
   
    

def create_frame2(): 
    window2_label = tkinter.ttk.Label(second_frame, text='劉さんのお勧め') 
    window2_label.grid(column=0, row=0, pady=10, padx=10, sticky=(tkinter.N)) 

    window2_back_button = tkinter.Button(second_frame, text = "Back", command = call_frame1) 
    window2_back_button.grid(column=0, row=1, pady=10, sticky=(tkinter.N)) 
    window2_next_button = tkinter.Button(second_frame, text = "Next", command = call_frame3) 
    window2_next_button.grid(column=1, row=1, pady=10, sticky=(tkinter.N)) 

def create_frame3(): 
    window3_label = tkinter.ttk.Label(third_frame, text='左さんのデータ') 
    window3_label.grid(column=0, row=0, pady=10, padx=10, sticky=(tkinter.N)) 
    window3_back_button = tkinter.Button(third_frame, text = "Back", command = call_frame2) 
    window3_back_button.grid(column=0, row=1, pady=10, sticky=(tkinter.N)) 
    window3_quit_button = tkinter.Button(third_frame, text = "Quit", command = quit_program) 
    window3_quit_button.grid(column=1, row=1, pady=10, padx=30,sticky=(tkinter.N)) 
    def print_checkboxA():
        var1 = tkinter.StringVar()
        var1.set(('池袋','大塚','巣鴨','駒込','田端','西日暮里','日暮里','鶯谷','上野','御徒町','秋葉原',
            '神田（東京）','東京','有楽町','新橋','浜松町','田町（東京）','品川','大崎','五反田','目黒',
            '恵比寿（東京）','渋谷','原宿','代々木','新宿','新大久保','高田馬場','目白'))
        enter_lb = tkinter.Listbox(third_frame,listvariable=var1)
        enter_lb.grid(column=0,row=3,pady=10,padx=20,sticky=(tkinter.N))
    window1_radiobutton1=tkinter.Radiobutton(third_frame,text='山手線',value = 'A',command=print_checkboxA)
    window1_rad
    iobutton1.grid(column=0, row=2, pady=10,padx=20,sticky=(tkinter.N))
    global img1
    canvas = tkinter.Canvas(third_frame,width = 500,height = 300,relief = tkinter.RIDGE,bd = 2)
    img1 = Image.open(open('icon.png','rb'))
    img1.thumbnail((500,500),Image.ANTIALIAS)
    img1 = ImageTk.PhotoImage(img1)
    canvas.create_image(10,10,image = img1,anchor = tkinter.NW) 
    canvas.grid()
    
def call_frame1(): 
    second_frame.grid_forget() 
    first_frame.grid(column=0, row=0, padx=20, pady=5, sticky=(tkinter.W, tkinter.N, tkinter.E)) 

def call_frame2(): 
    first_frame.grid_forget() 
    third_frame.grid_forget() 
    second_frame.grid(column=0, row=0, padx=20, pady=5, sticky=(tkinter.W, tkinter.N, tkinter.E)) 

def call_frame3(): 
    second_frame.grid_forget() 
    third_frame.grid(column=0, row=0, padx=20, pady=5, sticky=(tkinter.W, tkinter.N, tkinter.E)) 

def quit_program(): 
    root_window.destroy() 


    


root_window = tkinter.Tk() 
root_window.title('ネット　キャップター')
window_width = 400 
window_heigth = 600 

first_frame=tkinter.ttk.Frame(root_window, width=window_width, height=window_heigth) 
first_frame['borderwidth'] = 2 
first_frame['relief'] = 'sunken' 
first_frame.grid(column=0, row=0, padx=20, pady=5, sticky=(tkinter.W, tkinter.N, tkinter.E)) 

second_frame=tkinter.ttk.Frame(root_window, width=window_width, height=window_heigth) 
second_frame['borderwidth'] = 2 
second_frame['relief'] = 'sunken' 
second_frame.grid(column=0, row=0, padx=20, pady=5, sticky=(tkinter.W, tkinter.N, tkinter.E)) 

third_frame=tkinter.ttk.Frame(root_window, width=window_width, height=window_heigth) 
third_frame['borderwidth'] = 2 
third_frame['relief'] = 'sunken' 
third_frame.grid(column=0, row=0, padx=20, pady=5, sticky=(tkinter.W, tkinter.N, tkinter.E)) 

create_frame3() 
create_frame2() 
create_frame1() 

third_frame.grid_forget() 
second_frame.grid_forget() 

root_window.mainloop() 