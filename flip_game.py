from tkinter import *      
import time
import random


input("welcome to ピンボールゲーム")

tk=Tk()
tk.title("ピンボールゲーム") 
canvas=Canvas(tk,width=800,height=600,background="skyblue",bd=0,highlightthickness = 0) 
tk.resizable(0,0)
canvas.pack()
tk.update()

class Ball:
  def __init__(self,canvas,paddle,color): 
    self.canvas=canvas 
    self.paddle=paddle 
    self.id=canvas.create_oval(10,10,25,25,fill=color)
    self.canvas.move(self.id,240,100)
    stat=[-3,-2,-1,1,2,3]   
    random.shuffle(stat) 
    self.x=stat[0] 
    self.y=-3
    self.canvas_height=self.canvas.winfo_height()
    self.canvas_width=self.canvas.winfo_width() 
    self.hit_bottom=False 

  def p(self, pos): 
    paddle_pos = self.canvas.coords(self.paddle.id ) 
    if pos[2]>= paddle_pos[0] and pos[0]<= paddle_pos[2]: 
      if pos[3]>= paddle_pos[1] and pos[3]<= paddle_pos[3]: 
        return True
    return False


  def draw(self):
    self.canvas.move(self.id,self.x,self.y) 
    pos=self.canvas.coords(self.id) 
    if pos[1]<=0:
      self.y=3
    if pos[3]>=self.canvas_height: 
      self.hit_bottom=True
    if self.p(pos)==True:
      self.y=-3
    if pos[0]<=0: 
      self.x=3
    if pos[2]>=self.canvas_width: 
      self.x=-3


class Paddle:
  def __init__(self,canvans,color): 
    self.canvas=canvas 
    self.id=canvas.create_rectangle(0,0,150,10,fill=color) 
    self.canvas.move(self.id,400,450) 
    self.x=0
    self.canvas_width=self.canvas.winfo_width() 
    self.canvas.bind_all("<KeyPress-Left>",self.tleft)
    self.canvas.bind_all("<KeyPress-Right>", self.tright)
    
  def tleft(self,event):
    self.x=-5
  def tright(self,event): 
    self.x=5
  def draw(self):
    pos=self.canvas.coords(self.id) 
    self.canvas.move(self.id, self.x, 0) 
    if pos[0]<=0: 
      self.x=0
    if pos[2]>=self.canvas_width: 
      self.x=0
paddle=Paddle(canvas,"green") 
ball=Ball(canvas,paddle,"black") 
while True:
  if ball.hit_bottom==False: 
    ball.draw() 
    paddle.draw() 
  else: 
    break
  tk.update_idletasks()
  tk.update() 
  time.sleep(0.01)