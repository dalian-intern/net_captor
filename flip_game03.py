from tkinter import *
import time
import random



tk=Tk()
tk.title("ピンボールゲーム")
tk.wm_attributes('-topmost', 1)
background=Canvas(tk,width=600,height=400,background="pink",bd=5,highlightthickness = 0)
tk.resizable(0,0)
background.pack()
tk.update()

game_begin_text=background.create_text(290,200,text='welcome to ピンボールゲーム',font=('Times',40),state='hidden')
game_rule_text=background.create_text(450,40,text='ゲームルールを注意してください：',font=('Times',15),state='hidden')
game_start_text=background.create_text(415,70,text='DOWN:START',font=('Times',10),state='hidden')
game_end_text=background.create_text(400,80,text='UP:END',font=('Times',10),state='hidden')

game_over_text=background.create_text(100,50,text='GAME OVER',font=('Times',30),state='hidden')
game_start_text=background.create_text(100,50,text='GAME START',font=('Times',30),state='hidden')
game_score_text=background.create_text(400,90,text='SCORE:',font=('Times',10),state='hidden')



class plank:
  def __init__(self,background,c):
    self.background=background
    self.id=background.create_rectangle(0,0,100,10,fill=c)
    self.background.move(self.id,300,350)
    self.started=False
    self.x=0
    self.canvas_width=self.background.winfo_width()
    self.background.bind_all("<KeyPress-Down>", self.game_start)
    self.background.bind_all("<KeyPress-Left>",self.game_left)
    self.background.bind_all("<KeyPress-Right>", self.game_right)
    self.background.bind_all("<KeyPress-Up>", self.game_stop)


  def game_left(self,event):
    self.x=-3


  def game_right(self,event):
    self.x=3


  def game_start(self,evt):
    self.started=True

  def game_stop(self,evt):
    self.started = False


  def draw(self):
    pos=self.background.coords(self.id)
    self.background.move(self.id, self.x, 0)
    if pos[0]<=0:
      self.x=0
    if pos[2]>=self.canvas_width:
      self.x=0

class ball:
  def __init__(self,background,plank,c):
    self.background=background
    self.plank=plank
    self.id=background.create_oval(10,10,25,25,fill=c)
    self.background.move(self.id,245,100)
    stat=[-3,-2,-1,1,2,3]
    random.shuffle(stat)
    self.x=stat[0]
    self.y=-3
    self.canvas_height=self.background.winfo_height()
    self.canvas_width=self.background.winfo_width()
    self.hit_bottom=False


  def p(self, pos):
    paddle_pos = self.background.coords(self.plank.id )
    if pos[2]>= paddle_pos[0] and pos[0]<= paddle_pos[2]:
      if pos[3]>= paddle_pos[1] and pos[3]<= paddle_pos[3]:
        return True
    return False

  def hit_paddle(self,pos):
    paddle_pos=self.background.coords(self.plank.id)
    if pos[2]>=paddle_pos[0] and pos[0]<=paddle_pos[2]:
      if pos[3]>=paddle_pos[1] and pos[3]<=paddle_pos[3]:
        return True
    return False
  def draw(self):
    self.background.move(self.id,self.x,self.y)
    pos=self.background.coords(self.id)
    if pos[3]>=self.canvas_height:
        self.hit_bottom=True
    if self.hit_paddle(pos)==True:
        self.y=-3
        score.addscore()
    if pos[0]<=0:
        self.x+=1
    if pos[1]<=0:
        self.y=1
    if pos[2]>=600:
        self.x-=1
    if pos[3]>=self.canvas_height:
        self.y=-1
class Score:
  def __init__(self,background,color):
      self.score=0;
      self.background=background
      background.itemconfig(game_score_text, state='normal')
      self.id=background.create_text(470,90,text=self.score,fill=color)
      background.itemconfig(game_start_text, state='normal')
      background.itemconfig(game_end_text, state='normal')
      background.itemconfig(game_rule_text, state='normal')
      background.itemconfig(game_begin_text, state='normal')

  def addscore(self):
        self.score+=1
        self.background.itemconfig(self.id,text=self.score)

score=Score(background,'red')
plank=plank(background,"purple")
ball=ball(background,plank,"gray")


while 1:
  if ball.hit_bottom==False and plank.started==True:
    ball.draw()
    plank.draw()
    background.itemconfig(game_begin_text, state='hidden')
    background.itemconfig(game_start_text, state='normal')
    if ball.hit_bottom==True:
      background.itemconfig(game_over_text, state='normal')
      background.itemconfig(game_start_text, state='hidden')
  tk.update_idletasks()
  tk.update()
  time.sleep(0.008)
