import tkinter
import tkinter.ttk
from tkinter import * 
import time
import random
from PIL import Image,ImageTk


def create_frame1():
  
  window_label1 = tkinter.ttk.Label(first_frame,text="どちらに興味がありますか？",width=20)
  window_label1.grid(column=0, row=0, pady=10, padx=10, sticky=(tkinter.N))

  window1_quit_button = tkinter.Button(first_frame, text = "Quit", command = quit_program)
  window1_quit_button.grid(column=0, row=3, pady=4, sticky=(tkinter.N))
  window1_game_button = tkinter.Button(first_frame, text = "ゲームならここをクリック", command = flip_game)
  window1_game_button.grid(column=0, row=4, pady=4, sticky=(tkinter.N))
  window1_ok_button = tkinter.Button(first_frame, text = "OK", command = print_animal)
  window1_ok_button.grid(column=0, row=5, pady=4, sticky=(tkinter.N))




def print_animal():
  global img1
  tk1=Tk()
  tk1.title("ピンボール")
  tk1_frame = tkinter.ttk.Frame(tk1_frame,width=500,height=300)
  tk1_frame.grid()
  
  canvas = tkinter.Canvas(tk1_frame,width = 500,height = 300,relief = tkinter.RIDGE,bd = 2)
  img1 = Image.open(open('icon.png','rb'))
  img1.thumbnail((500,500),Image.ANTIALIAS)
  img1 = ImageTk.PhotoImage(img1)
  canvas.create_image(30,30,image = img1,anchor = tkinter.NW) 
  canvas.grid()


def flip_game():

  tk=Tk()
  tk.title("ピンボールゲーム")
  tk.wm_attributes('-topmost', 1)
  background=Canvas(tk,width=600,height=400,background="pink",bd=5,highlightthickness = 0)
  tk.resizable(0,0)
  background.pack()
  tk.update()

  game_startsigal_text=background.create_text(430,10,text='DOWN:START',font=('Times',10),state='hidden')
  game_endsigal_text=background.create_text(430,30,text='UP:END',font=('Times',10),state='hidden')
  game_score_text=background.create_text(430,90,text='SCORE:',font=('Times',10),state='hidden')

  game_over_text=background.create_text(250,200,text='GAME OVER',font=('Times',30),state='hidden')
  game_start_text=background.create_text(430,110,text='GAME START',font=('Times',10),state='hidden')



  class plank:
    def __init__(self,background,c):
      self.background=background
      self.id=background.create_rectangle(0,0,100,10,fill=c)
      self.background.move(self.id,300,350)
      self.started=False
      self.x=0
      self.canvas_width=self.background.winfo_width()
      self.background.bind_all("<KeyPress-Down>", self.game_start)
      self.background.bind_all("<KeyPress-Left>",self.game_left)
      self.background.bind_all("<KeyPress-Right>", self.game_right)
      self.background.bind_all("<KeyPress-Up>", self.game_stop)


    def game_left(self,event):
      self.x=-3


    def game_right(self,event):
      self.x=3


    def game_start(self,evt):
      self.started=True

    def game_stop(self,evt):
      self.started = False


    def draw(self):
      pos=self.background.coords(self.id)
      self.background.move(self.id, self.x, 0)
      if pos[0]<=0:
        self.x=0
      if pos[2]>=self.canvas_width:
        self.x=0

  class ball:
    def __init__(self,background,plank,c):
      self.background=background
      self.plank=plank
      self.id=background.create_oval(10,10,25,25,fill=c)
      self.background.move(self.id,145,100)
      stat=[-3,-2,-1,1,2,3]
      random.shuffle(stat)
      self.x=stat[0]
      self.y=-3
      self.canvas_height=self.background.winfo_height()
      self.canvas_width=self.background.winfo_width()
      self.hit_bottom=False


    def p(self, pos):
      paddle_pos = self.background.coords(self.plank.id )
      if pos[2]>= paddle_pos[0] and pos[0]<= paddle_pos[2]:
        if pos[3]>= paddle_pos[1] and pos[3]<= paddle_pos[3]:
          return True
      return False

    def hit_paddle(self,pos):
      paddle_pos=self.background.coords(self.plank.id)
      if pos[2]>=paddle_pos[0] and pos[0]<=paddle_pos[2]:
        if pos[3]>=paddle_pos[1] and pos[3]<=paddle_pos[3]:
          return True
      return False
    def draw(self):
      self.background.move(self.id,self.x,self.y)
      pos=self.background.coords(self.id)
      if pos[1]<=0:
        self.y=1
      if pos[3]>=self.canvas_height:
        self.hit_bottom=True
      if self.hit_paddle(pos)==True:
        self.y=-3
        score.addscore()
      if self.p(pos)==True:
        self.y=-1
      if pos[1]<=1:
        self.x=1
      if pos[2]>=self.canvas_width:
        self.x=-1
  class Score:
    def __init__(self,background,color):
        self.score=0;
        self.background=background
        background.itemconfig(game_score_text, state='normal')
        self.id=background.create_text(470,90,text=self.score,fill=color)
    def addscore(self):
          self.score+=1
          self.background.itemconfig(self.id,text=self.score)

  score=Score(background,'black')
  plank=plank(background,"purple")
  ball=ball(background,plank,"gray")


  while 1:
    if ball.hit_bottom==False and plank.started==True:
      ball.draw()
      plank.draw()
      background.itemconfig(game_start_text, state='normal')
      if ball.hit_bottom==True:
        background.itemconfig(game_over_text, state='normal')
        background.itemconfig(game_start_text, state='hidden')
    tk.update_idletasks()
    tk.update()
    time.sleep(0.008)
    
  
def quit_program():
  root_window.destroy()



root_window = tkinter.Tk()
root_window.title('ネット　キャップター')


first_frame=tkinter.ttk.Frame(root_window, width=2000, height=3000)
first_frame['borderwidth'] = 2
first_frame['relief'] = 'sunken'
first_frame.grid(column=0, row=0, padx=20, pady=5, sticky=(tkinter.W, tkinter.N, tkinter.E))


create_frame1()



root_window.mainloop()