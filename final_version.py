import tkinter
import tkinter.ttk
from tkinter import * 
import time
import random
from PIL import Image,ImageTk


def create_frame1():
  
  window_label1 = tkinter.ttk.Label(first_frame,text="どちらに興味がありますか？",width=20)
  window_label1.grid(column=1, row=0, pady=10, padx=10, sticky=(tkinter.N))

  

  global img2
  canvas = tkinter.Canvas(first_frame,width = 280,height = 200,relief = tkinter.RIDGE,bd = 2)
  img2 = Image.open(open('icon1.png','rb'))
  img2.thumbnail((500,300),Image.ANTIALIAS)
  img2 = ImageTk.PhotoImage(img2)
  canvas.create_image(10,10,image = img2,anchor = tkinter.NW) 
  canvas.grid()


  def print_listboxA():
    global enter_lb
    global seikatu_lb
    window_label1.config(text ="エンタテインメント")
    var1 = tkinter.StringVar()
    var1.set(("アニメ","ゲーム"))
    enter_lb = tkinter.Listbox(first_frame,listvariable=var1) 
    enter_lb.grid(column=1,row=1,pady=40,padx=80, sticky=(tkinter.N))
  
  
  def print_listboxB():
    global enter_lb
    global seikatu_lb
    window_label1.config(text = "生活")
    var2 = tkinter.StringVar()
    var2.set(("あつまる会社周辺地図","山手線各駅"))
    seikatu_lb = tkinter.Listbox(first_frame,listvariable=var2)
    seikatu_lb.grid(column=2,row=1,pady=40, padx=10, sticky=(tkinter.N))


  global var3
  var3 = tkinter.StringVar()
  window1_radiobutton1=tkinter.Radiobutton(first_frame,text="エンタテインメント",variable = var3,value = "A",command=print_listboxA)
  window1_radiobutton1.grid(column=1, row=1, pady=4, padx=0, sticky=(tkinter.N))

  global var4
  var4 = tkinter.StringVar()
  window1_radiobutton2 = tkinter.Radiobutton(first_frame,text="生活", variable = var4,value = "B",command = print_listboxB)
  window1_radiobutton2.grid(column=2, row=1, pady=4,padx=0,sticky=(tkinter.N))

  window1_quit_button = tkinter.Button(first_frame, text = "Quit", command = quit_program)
  window1_quit_button.grid(column=1, row=6, pady=4, sticky=(tkinter.N))
  window1_ok_button = tkinter.Button(first_frame, text = "OK", command = ok_info)
  window1_ok_button.grid(column=2, row=6, pady=4, sticky=(tkinter.N))
  

def print_animal():
	tk1 = tkinter.Toplevel(root_window)
	tk1.geometry("650x350+50+40")

	def close1():
		tk1.destroy()

	global img1
	img1 = ImageTk.PhotoImage(Image.open(open('icon.png','rb')))

	c1 = Button(tk1,image = img1,bg="black",command = close1)
	c1.grid(row=1)
	c2 = tkinter.Button(tk1,text="close",command=close1)
	c2.grid()
	canvas = tkinter.Canvas(tk1,width = 500,height = 300,relief = tkinter.RIDGE,bd = 2)
	canvas.create_image(0,0,image =img1,tag = "illust",anchor = tkinter.NW) 
	  

def senro():
  tk2=tkinter.Toplevel(root_window)
  tk2.geometry("650x350+50+40")

  window_label2 = tkinter.ttk.Label(tk2,text="山手線各駅",width=20)
  window_label2.grid(column=1, row=0, pady=10, padx=10, sticky=(tkinter.N))

  def close2():
    tk2.destroy()
  var3 = tkinter.StringVar()
  var3.set(('池袋','大塚','巣鴨','駒込','田端','西日暮里','日暮里','鶯谷','上野','御徒町','秋葉原',
            '神田（東京）','東京','有楽町','新橋','浜松町','田町（東京）','品川','大崎','五反田','目黒',
            '恵比寿（東京）','渋谷','原宿','代々木','新宿','新大久保','高田馬場','目白'))
  c4 = tkinter.Button(tk2,text="close",command=close2)
  c4.grid(column=1, row=2)
  yamanote_lb = tkinter.Listbox(tk2,listvariable=var3) 
  yamanote_lb.grid(column=1,row=1,pady=5,padx=5, sticky=(tkinter.N))


def print_atsumarumap():
	tk3 = tkinter.Toplevel(root_window)
	tk3.geometry("1000x700+50+40")

	def close3():
		tk3.destroy()

	global img3
	img3 = ImageTk.PhotoImage(Image.open(open('atsumaru_map.png','rb')))

	c5 = Button(tk3,image = img3,bg="black",command = close3)
	c5.grid(row=1)
	c6 = tkinter.Button(tk3,text="close",command=close3)
	c6.grid()
	canvas = tkinter.Canvas(tk3,width = 500,height = 300,relief = tkinter.RIDGE,bd = 2)
	canvas.create_image(0,0,image =img3,tag = "illust",anchor = tkinter.NW) 
	  

def flip_game():

  import time
  import random



  tk=Tk()
  tk.title("ピンボールゲーム")
  tk.wm_attributes('-topmost', 1)
  background=Canvas(tk,width=600,height=400,background="pink",bd=0,highlightthickness = 0)
  tk.resizable(0,0)
  background.pack()
  tk.update()

  game_begin_text=background.create_text(290,200,text='welcome to ピンボールゲーム',font=('Times',40),state='hidden')
  game_rule_text=background.create_text(450,40,text='ゲームルールを注意してください：',font=('Times',15),state='hidden')
  game_startsigal_text=background.create_text(415,70,text='DOWN:START',font=('Times',10),state='hidden')
  game_endsigal_text=background.create_text(400,80,text='UP:END',font=('Times',10),state='hidden')

  game_over_text=background.create_text(100,50,text='GAME OVER',font=('Times',30),state='hidden')
  game_start_text=background.create_text(100,50,text='GAME START',font=('Times',30),state='hidden')
  game_score_text=background.create_text(400,90,text='SCORE:',font=('Times',10),state='hidden')



  class plank:
    def __init__(self,background,c):
      self.background=background
      self.id=background.create_rectangle(0,0,100,10,fill=c)
      self.background.move(self.id,300,350)
      self.started=False
      self.x=0
      self.canvas_width=self.background.winfo_width()
      self.background.bind_all("<KeyPress-Down>", self.game_start)
      self.background.bind_all("<KeyPress-Left>",self.game_left)
      self.background.bind_all("<KeyPress-Right>", self.game_right)
      self.background.bind_all("<KeyPress-Up>", self.game_stop)


    def game_left(self,event):
      self.x=-3


    def game_right(self,event):
      self.x=3


    def game_start(self,evt):
      self.started=True

    def game_stop(self,evt):
      self.started = False


    def draw(self):
      pos=self.background.coords(self.id)
      self.background.move(self.id, self.x, 0)
      if pos[0]<=0:
        self.x=0
      if pos[2]>=self.canvas_width:
        self.x=0

  class ball:
    def __init__(self,background,plank,c):
      self.background=background
      self.plank=plank
      self.id=background.create_oval(10,10,25,25,fill=c)
      self.background.move(self.id,245,100)
      stat=[-3,-2,-1,1,2,3]
      random.shuffle(stat)
      self.x=stat[0]
      self.y=-3
      self.canvas_height=self.background.winfo_height()
      self.canvas_width=self.background.winfo_width()
      self.hit_bottom=False


    def p(self, pos):
      paddle_pos = self.background.coords(self.plank.id )
      if pos[2]>= paddle_pos[0] and pos[0]<= paddle_pos[2]:
        if pos[3]>= paddle_pos[1] and pos[3]<= paddle_pos[3]:
          return True
      return False

    def hit_paddle(self,pos):
      paddle_pos=self.background.coords(self.plank.id)
      if pos[2]>=paddle_pos[0] and pos[0]<=paddle_pos[2]:
        if pos[3]>=paddle_pos[1] and pos[3]<=paddle_pos[3]:
          return True
      return False
    def draw(self):
      self.background.move(self.id,self.x,self.y)
      pos=self.background.coords(self.id)
      if pos[3]>=self.canvas_height:
          self.hit_bottom=True
      if self.hit_paddle(pos)==True:
          self.y=-3
          score.addscore()
      if pos[0]<=0:
          self.x+=1
      if pos[1]<=0:
          self.y=1
      if pos[2]>=600:
          self.x-=1
      if pos[3]>=self.canvas_height:
          self.y=-1
  class Score:
    def __init__(self,background,color):
        self.score=0;
        self.background=background
        background.itemconfig(game_score_text, state='normal')
        self.id=background.create_text(470,90,text=self.score,fill=color)
        background.itemconfig(game_startsigal_text, state='normal')
        background.itemconfig(game_endsigal_text, state='normal')
        background.itemconfig(game_rule_text, state='normal')
        background.itemconfig(game_begin_text, state='normal')

    def addscore(self):
          self.score+=1
          self.background.itemconfig(self.id,text=self.score)

  score=Score(background,'red')
  plank=plank(background,"purple")
  ball=ball(background,plank,"gray")


  while 1:
    if ball.hit_bottom==False and plank.started==True:
      ball.draw()
      plank.draw()
      background.itemconfig(game_start_text, state='normal')
      background.itemconfig(game_begin_text, state='hidden')
      if ball.hit_bottom==True:
        background.itemconfig(game_over_text, state='normal')
        background.itemconfig(game_start_text, state='hidden')
    tk.update_idletasks()
    tk.update()
    time.sleep(0.008)
    
  
def quit_program():
  root_window.destroy()

def ok_info():
	try:
	    for s in var3.get():
	      if s=="A":
	        for i in enter_lb.curselection():
	          enter_list = enter_lb.get(i)
	          if enter_list == "ゲーム":
	            flip_game()
	          elif enter_list == "アニメ":
	            print_animal()
	      else:
	        pass
	except:
	    pass
	try:
	    for c in var4.get():
	      if c=="B":
	        for h in seikatu_lb.curselection():
	          seikatu_list = seikatu_lb.get(h)
	          if seikatu_list == "あつまる会社周辺地図":
	            print_atsumarumap()
	          elif seikatu_list == "山手線各駅":
	            senro()
	      else:
	        pass
	except:
		pass

root_window = tkinter.Tk()
root_window.title('ネットキャップター')
root_window.geometry("900x400")

first_frame=tkinter.ttk.Frame(root_window, width=900, height=400)
first_frame['borderwidth'] = 2
first_frame['relief'] = 'sunken'
first_frame.grid(column=0, row=0, padx=5, pady=5, sticky=(tkinter.W, tkinter.N, tkinter.E))
first_frame.grid_propagate(0)

create_frame1()

root_window.mainloop()